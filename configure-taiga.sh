#!/bin/sh

settings_file=/usr/share/taiga-back/settings/local.py

# Set default values
DB_NAME=${DB_NAME-taiga}
DB_USER=${DB_USER-taiga}
EMAIL_USE_TLS=${EMAIL_USE_TLS-True}


# Subsitute the variables in the settings file template

# Database Settings
sed -i s/\${DB_NAME}/${DB_NAME}/ $settings_file
sed -i s/\${DB_USER}/${DB_USER}/ $settings_file
sed -i s/\${DB_PASSWORD}/${DB_PASSWORD}/ $settings_file
sed -i s/\${DB_HOST}/${DB_HOST}/ $settings_file
sed -i s/\${DB_PORT}/${DB_PORT}/ $settings_file

# Site Domain Settings
sed -i s/\${SITE_PROTO}/${SITE_PROTO}/ $settings_file
sed -i s/\${SITE_DOMAIN}/${SITE_DOMAIN}/ $settings_file
sed -i s/\${SITE_PORT}/${SITE_PORT}/ $settings_file

# Cellery Settings
sed -i s/\${CELERY_ENABLED}/${CELERY_ENABLED}/ $settings_file

# Email Settings
sed -i s/\${DEFAULT_FROM_EMAIL}/${DEFAULT_FROM_EMAIL}/ $settings_file

if [ -n "$EMAIL_HOST" ]; then # If there is passed-in email configuration
    # Uncomment the lines in the settings file
    sed -i s/\#EMAIL_USE_TLS/EMAIL_USE_TLS/ $settings_file
    sed -i s/\#EMAIL_HOST/EMAIL_HOST/ $settings_file
    sed -i s/\#EMAIL_PORT/EMAIL_PORT/ $settings_file
    sed -i s/\#EMAIL_HOST_USER/EMAIL_HOST_USER/ $settings_file
    sed -i s/\#EMAIL_HOST_PASSWORD/EMAIL_HOST_PASSWORD/ $settings_file

    # Substitute the variables into the settings file
    sed -i s/\${EMAIL_USE_TLS}/${EMAIL_USE_TLS}/ $settings_file
    sed -i s/\${EMAIL_HOST}/${EMAIL_HOST}/ $settings_file
    sed -i s/\${EMAIL_PORT}/${EMAIL_PORT}/ $settings_file
    sed -i s/\${EMAIL_HOST_USER}/${EMAIL_HOST_USER}/ $settings_file
    sed -i s/\${EMAIL_HOST_PASSWORD}/${EMAIL_HOST_PASSWORD}/ $settings_file
fi

# Make sure we own the media direcotry just in case it is a mounted volume
chown -R nginx /usr/share/taiga-back/media
